import React from 'react';
import {Button} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";


const UserMenu = ({user}) => {
    const dispatch = useDispatch();

    return (
        <>
            <Button aria-controls="simple-menu" aria-haspopup="true" color="inherit">
                Hello, {user.username}!
            </Button>
            <Button onClick={() => dispatch(logoutUser())} color="inherit">Logout</Button>
        </>
    );
};

export default UserMenu;