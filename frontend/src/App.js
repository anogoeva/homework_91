import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import Chat from "./containers/Chat/Chat";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };
    return (

        <Layout>
            <Switch>
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <ProtectedRoute
                    path="/"
                    component={Chat}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <Route path="/" exact component={Chat}/>
                {/*<Route path="/products/:id" component={Product}/>*/}
            </Switch>
        </Layout>

    );
};

export default App;
