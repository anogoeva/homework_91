import React, {useEffect, useRef, useState} from 'react';
import {useSelector} from "react-redux";
import {Grid, makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    border:
        {
            border: "2px solid lightGrey",
            borderRadius: "5px",
            padding: "25px 300px"
        },
    usersBorder:
        {
            border: "2px solid lightGrey",
            borderRadius: "5px",
            padding: "25px 100px",
        }
}));

const Chat = () => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState('');
    const [usernames, setUsernames] = useState([]);

    const ws = useRef(null);

    useEffect(() => {
        const token = user.token;
        ws.current = new WebSocket('ws://localhost:8000/chat?token=' + token);
        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);

            if (decoded.type === 'NEW_MESSAGE') {
                setMessages(prev => [
                    ...prev,
                    decoded.messages,
                ]);
            }

            if (decoded.type === 'CONNECTED') {
                setUsernames(decoded.usersArray);
                const messages = decoded.messages;
                setMessages(messages);
                ws.current.send(JSON.stringify({
                    type: 'NEW_CONNECTED',
                    usersArray: usernames,
                }));
            }
        };
    }, []);

    const sendMessage = () => {
        ws.current.send(JSON.stringify({
            type: 'CREATE_MESSAGE',
            message,
        }));
        setMessage('');
    };
    return (
        <>
            <Grid container justifyContent="space-around">
                <Grid item>
                    <h1>USERS</h1>
                    <div>
                        <h4>List of users</h4>
                        {usernames.map((username, i) => (
                            <div key={i}>
                                <p className={classes.usersBorder}>{username.username}<br/></p>
                            </div>
                        ))}
                    </div>
                </Grid>
                <Grid item>
                    <h1>CHAT ROOM</h1>
                    <>
                        <h3>List of messages</h3>
                        {messages.map((msg, i) => (
                            <p className={classes.border} key={i}>
                                <b>{msg.username ? msg.username : msg.user.username}: </b>{msg.message ? msg.message : msg.text}
                            </p>
                        ))}
                    </>
                    <p>
                        <input
                            type="text"
                            value={message}
                            onChange={e => setMessage(e.target.value)}
                        />
                    </p>
                    <button onClick={sendMessage}>Send message</button>

                </Grid>
            </Grid>
        </>
    );
};

export default Chat;