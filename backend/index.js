const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const users = require('./app/users');
const config = require('./config');
const User = require("./models/User");
const Message = require("./models/Message");

const app = express();
require('express-ws')(app);

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8000;
const activeConnections = {};


const usersArray = [];
app.ws('/chat', async (ws, req) => {
  let username = null;
  let userId = null;
  const token = req.query.token;

  try {
    const user = await User.findOne({token: token});

    userId = user._id;
    username = user.username;
    usersArray.push({username: user.username, id: userId});

    activeConnections[userId] = ws;

    ws.send(JSON.stringify({
      type: 'CONNECTED',
      usersArray,
      messages: await Message.find().populate('user').sort({date: -1}).limit(30),
    }));

    ws.on('close', () => {
      console.log(`Client disconnected! id=${userId}`);
      delete activeConnections[userId];
      usersArray.splice(usersArray.findIndex(e => e.id === userId), 1);

      Object.keys(activeConnections).forEach(key => {

        const connection = activeConnections[key];е
        connection.send(JSON.stringify({
          type: 'DISCONNECTED',
          usersArray
        }));
      })
    });

    console.log(`Client connected with id = ${userId}`);
  } catch (error) {
    ws.send(JSON.stringify({
      type: 'ERROR',
      message: error,
    }));
  }

  ws.on('message', msg => {

    const decoded = JSON.parse(msg);

    switch (decoded.type) {

      case 'CREATE_MESSAGE':
        Object.keys(activeConnections).forEach(key => {
          const connection = activeConnections[key];
          connection.send(JSON.stringify({
            type: 'NEW_MESSAGE',
            messages: {
              username,
              text: decoded.message
            },
          }));

          const messageData = {
            message: decoded.message,
            user: userId,
            date: new Date() || null,
          };

          const message = new Message(messageData);
          message.save();
        })
        break;

      case 'NEW_CONNECTED':
        Object.keys(activeConnections).forEach(key => {
          const connection = activeConnections[key];
          connection.send(JSON.stringify({
            type: 'REFRESH_CONNECTED',
          }));
        })
        break;

      default:
        console.log('Unknown type: ', decoded.type);
    }
  });
});

app.use('/users', users);
const run = async () => {
  await mongoose.connect(config.db.url);

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(() => {
    console.log('exiting');
    mongoose.disconnect();
  });
};

run().catch(e => console.error(e));
